fn main() {
    println!("cargo:rerun-if-changed=build.rs");

    std::fs::create_dir_all("elpa/m4").expect("failed to create dir elpa/m4");

    let test_programs_am = std::fs::File::create("elpa/test_programs.am")
        .expect("cannot create elpa/test_programs.am");
    let test_programs_am = std::process::Stdio::from(test_programs_am);
    std::process::Command::new("python3")
            .args(["elpa/generate_automake_test_programs.py"])
			.stdout(test_programs_am)
			.spawn()
			.expect("failed to run python script: python3 elpa/generate_automake_test_programs.py > elpa/test_programs.am");

    let mut cfg = autotools::Config::new("elpa");
    let dst = cfg
        .reconf("-iv")
        .with("mpi", Some("no"))
        .disable("avx512", None)
        .enable("openmp", None)
        .cflag("-march=native")
        .env("CPP", "cpp -E")
        .fast_build(true)
        .make_target("libelpa_onenode_openmp.la")
        .build();
    // skipped install step due to dumb fortran error in tests

    println!(
        "cargo:rustc-link-search=native={}/build/.libs",
        dst.display()
    ); // TODO: delete `build/.libs' if `install' is fixed
    println!("cargo:rustc-link-lib=static=elpa_onenode_openmp");
    println!("cargo:rustc-link-lib=gfortran");
    println!("cargo:rustc-link-lib=openblas");
    println!("cargo:rustc-link-lib=gomp");
}
