pub const ELPA_API_VERSION: i32 = 20220501;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ZgemmChar {
    UpperTriangle,
    LowerTriangle,
    AllMatrix
}

#[repr(i32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Solver {
    Solver1Stage = 1,
    Solver2Stage = 2
}

#[repr(i32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum AutotuneLevel {
    NotTunable = 0,
    Gpu = 1,
    Kernel = 2,
    Openmp = 3,
    TransposeVectors = 4,
    FullToBand = 5,
    BandToTridi = 6,
    Solve = 7,
    TridiToBand = 8,
    BandToFull = 9,
    Main = 10,
    FullToTridi = 11,
    TridiToFull = 12,
    Mpi = 13,
    Fast = 14,
    Medium = 15,
    BandToFullBlocking = 16,
    MaxStoredRows = 17,
    TridiToBandStripewidth = 18,
    Extensive = 19,
}

#[repr(i32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum AutotuneDomain {
    Real = 1,
    Complex = 2,
    Any = 3
}

#[repr(i32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ErrorCode {
    ErrorDuringComputation = -13,
    CannotOpenFile = -12,
    EntryReadonly = -11,
    AutotuneObjectChanged = -10,
    AutotuneApiVersion = -9,
    ApiVersion = -8,
    Critical = -7,
    Setup = -6,
    EntryNoStringRepresentation = -5,
    EntryAlreadySet = -4,
    EntryInvalidValue = -3,
    EntryNotFound = -2,
    ElpaError = -1,
    ElpaOk = 0
}

impl TryFrom<i32> for ErrorCode {
    type Error = ();

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        use ErrorCode::*;

        let ret = match value {
            -13 => ErrorDuringComputation,
            -12 => CannotOpenFile,
            -11 => EntryReadonly,
            -10 => AutotuneObjectChanged,
            -9  => AutotuneApiVersion,
            -8  => ApiVersion,
            -7  => Critical,
            -6  => Setup,
            -5  => EntryNoStringRepresentation,
            -4  => EntryAlreadySet,
            -3  => EntryInvalidValue,
            -2  => EntryNotFound,
            -1  => ElpaError,
            0   => ElpaOk,
            _   => return Err(())
        };

        Ok(ret)
    }
}

// From docs of subroutine elpa_hermitian_multiply_a_h_a_...
// For input matrix A
//     'U' if A is upper triangular
//     'L' if A is lower triangular
//     anything else if A is a full matrix
//     Please note: This pertains to the original A (as set in the calling program)
//                  whereas the transpose of A is used for calculations
//     If uplo_a is 'U' or 'L', the other triangle is not used at all,
//     i.e. it may contain arbitrary numbers
//
// For output matrix C
//     'U' if only the upper diagonal part of C is needed
//     'L' if only the upper diagonal part of C is needed
//     anything else if the full matrix C is needed
//     Please note: Even when uplo_c is 'U' or 'L', the other triangle may be
//                   written to a certain extent, i.e. one shouldn't rely on the content there!
impl From<ZgemmChar> for std::os::raw::c_char {
    fn from(value: ZgemmChar) -> Self {
        let ret = match value {
            ZgemmChar::UpperTriangle => 'U',
            ZgemmChar::LowerTriangle => 'L',
            ZgemmChar::AllMatrix => 'A',
        };

        ret as Self
    }
}