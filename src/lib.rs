mod consts;
#[cfg(test)]
mod tests;

use std::os::raw::{c_char, c_double, c_int};
use consts::{AutotuneDomain, AutotuneLevel, ErrorCode, Solver};

pub use consts::ZgemmChar;

#[allow(non_camel_case_types)]
pub type c64 = num_complex::Complex<f64>;

ffi_opaque::opaque! {
    #[allow(non_camel_case_types)]
    struct elpa_t;
    #[allow(non_camel_case_types)]
    struct elpa_autotune_t;
}

pub struct ElpaInstanceBuilder<'a> {
    ptr: *mut elpa_t,
    _m: std::marker::PhantomData<&'a elpa_t>,
}

pub struct ElpaInstance<'a> {
    ptr: *mut elpa_t,
    _m: std::marker::PhantomData<&'a elpa_t>,
}

pub struct ElpaAutotune<'a> {
    ptr: *mut elpa_autotune_t,
    _m: std::marker::PhantomData<&'a elpa_autotune_t>,
}


#[derive(Debug)]
pub struct ElpaError {
    code: ErrorCode,
    msg: String,
}

fn elpa_result<T>(wrap: T, ecode: i32) -> Result<T, ElpaError> {
    const ELPA_OK: i32 = ErrorCode::ElpaOk as i32;

    match ecode {
        ELPA_OK => Ok(wrap),
        _ => Err(ecode.try_into().unwrap()),
    }
}

pub fn try_init() -> Result<(), ElpaError> {
    elpa_result((), unsafe {
        elpa_init(consts::ELPA_API_VERSION).try_into().unwrap()
    })
}

pub fn try_uninit() -> Result<(), ElpaError> {
    let mut error_code = 0;
    unsafe { elpa_uninit(&mut error_code) };
    elpa_result((), error_code)
}

impl<'a> ElpaInstance<'a> {
    pub fn try_autotune_setup(
        &mut self,
        level: AutotuneLevel,
        domain: AutotuneDomain,
    ) -> Result<ElpaAutotune<'a>, ElpaError> {
        let mut error_code = 0;

        let ptr =
            unsafe { elpa_autotune_setup(self.ptr, level as i32, domain as i32, &mut error_code) };
        let ret = ElpaAutotune {
            ptr,
            _m: Default::default(),
        };
        elpa_result(ret, error_code)
    }

    pub fn try_autotune_step(&mut self, tuner: &mut ElpaAutotune) -> Result<bool, ElpaError> {
        let mut error_code = 0;

        let is_finished = unsafe { elpa_autotune_step(self.ptr, tuner.ptr, &mut error_code) };
        elpa_result(is_finished, error_code)
    }

    pub fn try_autotune_set_best(&mut self, tuner: &mut ElpaAutotune) -> Result<(), ElpaError> {
        let mut error_code = 0;

        unsafe { elpa_autotune_set_best(self.ptr, tuner.ptr, &mut error_code) };
        elpa_result((), error_code)
    }

    pub fn try_autotune_print_best(&mut self, tuner: &ElpaAutotune) -> Result<(), ElpaError> {
        let mut error_code = 0;

        unsafe { elpa_autotune_print_best(self.ptr, tuner.ptr, &mut error_code) };
        elpa_result((), error_code)
    }

    pub fn try_autotune_print_state(&mut self, tuner: &ElpaAutotune) -> Result<(), ElpaError> {
        let mut error_code = 0;

        unsafe { elpa_autotune_print_state(self.ptr, tuner.ptr, &mut error_code) };
        elpa_result((), error_code)
    }

    pub fn try_autotune_save_state(
        &mut self,
        filename: &std::path::PathBuf,
        tuner: &ElpaAutotune,
    ) -> Result<(), ElpaError> {
        let file_cstr = std::ffi::CString::new(filename.to_str().unwrap()).unwrap();
        let mut error_code = 0;

        unsafe {
            elpa_autotune_save_state(self.ptr, tuner.ptr, file_cstr.as_ptr(), &mut error_code)
        };
        elpa_result((), error_code)
    }

    pub fn try_autotune_load_state(
        &mut self,
        filename: &std::path::PathBuf,
        tuner: &ElpaAutotune,
    ) -> Result<(), ElpaError> {
        let file_cstr = std::ffi::CString::new(filename.to_str().unwrap()).unwrap();
        let mut error_code = 0;

        unsafe {
            elpa_autotune_load_state(self.ptr, tuner.ptr, file_cstr.as_ptr(), &mut error_code)
        };
        elpa_result((), error_code)
    }

    pub fn eigvec_generalized(
        &mut self,
        a: &mut [c64],
        b: &mut [c64],
        is_b_chol: bool,
        evec: &mut [c64],
        eval: &mut [f64],
    ) -> Result<(), ElpaError> {
        debug_assert!(a.len() == evec.len());
        debug_assert!(b.len() == evec.len());

        let mut error_code = 0;
        unsafe {
            elpa_generalized_eigenvectors_dc(
                self.ptr,
                a.as_mut_ptr(),
                b.as_mut_ptr(),
                eval.as_mut_ptr(),
                evec.as_mut_ptr(),
                is_b_chol.into(),
                &mut error_code,
            );
        }

        elpa_result((), error_code)
    }

    pub fn eigvec(
        &mut self,
        a: &mut [c64],
        evec: &mut [c64],
        eval: &mut [f64],
    ) -> Result<(), ElpaError> {
        debug_assert!(a.len() == evec.len());

        let mut error_code = 0;
        unsafe {
            elpa_eigenvectors_a_h_a_dc(
                self.ptr,
                a.as_mut_ptr(),
                eval.as_mut_ptr(),
                evec.as_mut_ptr(),
                &mut error_code,
            );
        }

        elpa_result((), error_code)
    }

    pub fn eigval(&mut self, a: &mut [c64], eval: &mut [f64]) -> Result<(), ElpaError> {
        let mut error_code = 0;
        unsafe {
            elpa_eigenvalues_a_h_a_dc(self.ptr, a.as_mut_ptr(), eval.as_mut_ptr(), &mut error_code);
        }

        elpa_result((), error_code)
    }

    pub fn zgemm_ad(
        &mut self,
        b_c_ncols: usize,
        a: &[c64],
        uplo_a: ZgemmChar,
        b: &[c64],
        c: &mut [c64],
        uplo_c: ZgemmChar,
    ) -> Result<(), ElpaError> {
        debug_assert!(b.len() == c.len());

        let b_c_nrows = self.try_get_integer("na")?;

        let nr_bc = b_c_nrows as i32;
        let nc_bc = b_c_ncols as i32;

        let mut error_code = 0;
        unsafe {
            elpa_hermitian_multiply_a_h_a_dc(
                self.ptr,
                uplo_a.into(),
                uplo_c.into(),
                nc_bc,
                a.as_ptr(),
                b.as_ptr(),
                nr_bc,
                nc_bc,
                c.as_mut_ptr(),
                nr_bc,
                nc_bc,
                &mut error_code,
            );
        }

        elpa_result((), error_code)
    }

    fn try_get_integer(&self, key: &str) -> Result<i32, ElpaError> {
        let key_cstr = std::ffi::CString::new(key).unwrap();
        let mut ret = 0;
        let mut error_code = 0;
        unsafe {
            elpa_get_integer(self.ptr, key_cstr.as_ptr(), &mut ret, &mut error_code);
        }
        elpa_result(ret, error_code)
    }
}

impl<'a> ElpaInstanceBuilder<'a> {
    pub fn try_new(matrix_size: usize, num_eigenvectors: usize) -> Result<Self, ElpaError> {
        let mut ret = {
            let mut error_code = 0;
            let ptr = unsafe { elpa_allocate(&mut error_code) };
            let ret = Self {
                ptr,
                _m: Default::default(),
            };
            elpa_result(ret, error_code)?
        };

        let na = matrix_size as i32;
        let nev = num_eigenvectors as i32;

        ret.try_set_integer("na", na)?;
        ret.try_set_integer("nev", nev)?;
        ret.try_set_integer("local_nrows", na)?;
        ret.try_set_integer("local_ncols", na)?;

        Ok(ret)
    }

    pub fn try_set_integer(&mut self, key: &str, value: i32) -> Result<&mut Self, ElpaError> {
        let key_cstr = std::ffi::CString::new(key).unwrap();
        let mut error_code = 0;
        unsafe {
            elpa_set_integer(self.ptr, key_cstr.as_ptr(), value, &mut error_code);
        }
        elpa_result(self, error_code)
    }

    pub fn try_is_set_key(&self, key: &str) -> Result<bool, ElpaError> {
        let key_cstr = std::ffi::CString::new(key).unwrap();
        let ret = unsafe { elpa_index_int_value_is_set(self.ptr, key_cstr.as_ptr()) };

        match ret {
            1 => Ok(true),
            0 => Ok(false),
            x if x < 0 => Err(ElpaError::try_from(x).unwrap()),
            _ => unreachable!(),
        }
    }

    pub fn try_set_blocksize(&mut self, nblk: usize) -> Result<&mut Self, ElpaError> {
        Ok(self.try_set_integer("nblk", nblk as i32)?)
    }

    pub fn try_set_omp_threads(&mut self, threads: usize) -> Result<&mut Self, ElpaError> {
        Ok(self.try_set_integer("omp_threads", threads as i32)?)
    }

    pub fn try_set_solver(&mut self, solver: Solver) -> Result<&mut Self, ElpaError> {
        Ok(self.try_set_integer("solver", solver as i32)?)
    }

    pub fn try_setup<'b>(mut self) -> Result<ElpaInstance<'b>, ElpaError> {
        if !self.try_is_set_key("omp_threads")? {
            self.try_set_omp_threads(1)?;
        }

        /* elpa bug: this is correct semantically
        but does not work because elpa _always_ returns that
        nblk is set, however, it defaults to 0 and one receives SIGFPE
        (integer division by zero) if nblk is not set manually */

        //if !self.try_is_set_key("nblk")? {
        //    self.try_set_blocksize(256)?;
        //}

        if let Err(error) = self.try_set_blocksize(256) {
            if error.code != ErrorCode::EntryAlreadySet {
                return Err(error);
            }
        }

        let error_code = unsafe { elpa_setup(self.ptr) };
        let ret = ElpaInstance {
            ptr: self.ptr,
            _m: Default::default(),
        };
        elpa_result(ret, error_code)
    }
}

impl Drop for ElpaInstance<'_> {
    fn drop(&mut self) {
        let mut error_code = 0;
        unsafe { elpa_deallocate(self.ptr, &mut error_code) };
        elpa_result((), error_code)
            .expect("bug: elpa: deallocate() failed while Drop was in progress");
    }
}

impl std::error::Error for ElpaError {}

impl std::fmt::Display for ElpaError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let code = self.code as i32;
        let msg = &self.msg;
        let msg = format!("elpa({code}): {msg}");
        std::fmt::Display::fmt(&msg, f)
    }
}

impl TryFrom<i32> for ElpaError {
    type Error = ();

    fn try_from(error_code: i32) -> Result<Self, Self::Error> {
        let ptr = unsafe { elpa_strerr(error_code) };
        let c_str = unsafe { std::ffi::CStr::from_ptr(ptr) }.to_str();
        let e_str = c_str
            .unwrap_or_else(|_| panic!("bug: elpa({error_code}): error string conversion failed"));

        let error_code = error_code.try_into()?;

        Ok(Self {
            code: error_code,
            msg: e_str.to_owned(),
        })
    }
}

#[link(name = "elpa_onenode_openmp", kind = "static")]
extern "C" {
    fn elpa_strerr(elpa_error: c_int) -> *const c_char;
    fn elpa_init(api_version: c_int) -> c_int;
    fn elpa_uninit(error: *mut c_int);
    fn elpa_allocate(error: *mut c_int) -> *mut elpa_t;
    fn elpa_deallocate(handle: *const elpa_t, error: *mut c_int);
    fn elpa_setup(handle: *mut elpa_t) -> c_int;
    fn elpa_set_integer(handle: *mut elpa_t, name: *const c_char, value: c_int, error: *mut c_int);
    fn elpa_get_integer(
        handle: *mut elpa_t,
        name: *const c_char,
        value: *mut c_int,
        error: *mut c_int,
    );
    //fn elpa_set_double(handle: *mut elpa_t, name: *const c_char, value: c_double, error: *mut c_int);
    fn elpa_index_int_value_is_set(handle: *mut elpa_t, name: *const c_char) -> i32;
    //fn elpa_can_set(handle: *mut elpa_t, name: *const c_char, value: c_int) -> i32;

    fn elpa_hermitian_multiply_a_h_a_dc(
        handle: *mut elpa_t,
        uplo_a: c_char,
        uplo_c: c_char,
        ncb: c_int,
        a: *const c64,
        b: *const c64,
        nr_b: c_int,
        nc_b: c_int,
        c: *mut c64,
        nr_c: c_int,
        nc_c: c_int,
        error: *mut c_int,
    );

    fn elpa_generalized_eigenvectors_dc(
        handle: *mut elpa_t,
        a: *mut c64,
        b: *mut c64,
        ev: *mut c_double,
        q: *mut c64,
        b_chol: c_int,
        error: *mut c_int,
    );

    fn elpa_eigenvectors_a_h_a_dc(
        handle: *mut elpa_t,
        a: *mut c64,
        ev: *mut c_double,
        q: *mut c64,
        error: *mut c_int,
    );

    fn elpa_eigenvalues_a_h_a_dc(
        handle: *mut elpa_t,
        a: *mut c64,
        ev: *mut c_double,
        error: *mut c_int,
    );

    fn elpa_autotune_setup(
        handle: *mut elpa_t,
        level: c_int,
        domain: c_int,
        error: *mut c_int,
    ) -> *mut elpa_autotune_t;

    fn elpa_autotune_step(
        handle: *mut elpa_t,
        tune_state: *mut elpa_autotune_t,
        error: *mut c_int,
    ) -> bool;

    fn elpa_autotune_set_best(
        handle: *mut elpa_t,
        tune_state: *mut elpa_autotune_t,
        error: *mut c_int,
    );

    fn elpa_autotune_print_best(
        handle: *mut elpa_t,
        tune_state: *const elpa_autotune_t,
        error: *mut c_int,
    );

    fn elpa_autotune_print_state(
        handle: *mut elpa_t,
        tune_state: *const elpa_autotune_t,
        error: *mut c_int,
    );

    fn elpa_autotune_save_state(
        handle: *mut elpa_t,
        tune_state: *const elpa_autotune_t,
        file_name: *const c_char,
        error: *mut c_int,
    );

    fn elpa_autotune_load_state(
        handle: *mut elpa_t,
        tune_state: *const elpa_autotune_t,
        file_name: *const c_char,
        error: *mut c_int,
    );
}
