// The purpose of this test is to check ELPA against precomputed values direcly
// without relying on the `nalgebra' package to do the math. If under any circumstances
// nalgebra should break the math, this test is the last resort.
mod small_matrix4 {
    #![allow(non_upper_case_globals)]
    use crate::{c64, try_init, try_uninit, ElpaInstanceBuilder};
    use nalgebra::{matrix, vector, Matrix4, Vector4};

    const I: c64 = c64::new(0., 1.);

    const a_re: Matrix4<f64> = matrix![
        0.966114063097475, 0.630900449680684, 0.648283748496811, 0.820107398379367;
        0.630900449680684, 0.903315852581357, 0.477655392047180, 0.807972806678453;
        0.648283748496811, 0.477655392047180, 0.319013705588855, 0.257037326083426;
        0.820107398379367, 0.807972806678453, 0.257037326083426, 0.846160798223660];

    const a_im: Matrix4<f64> = matrix![
        0.000000000000000,-0.108768516272244, 0.242502976531341,-0.008413015092325;
        0.108768516272244, 0.000000000000000,-0.025138723205731,-0.012747752958185;
        -0.242502976531341,0.025138723205731, 0.000000000000000,-0.011115700734270;
        0.008413015092325, 0.012747752958185, 0.011115700734270, 0.000000000000000];

    const b_re: Matrix4<f64> = matrix![
        1.025353945784540, 0.307433687723064, 0.346138383288846, 0.139722116114900;
        0.307433687723064, 1.218189773270965, 0.573938580966287, 0.564521020333593;
        0.346138383288846, 0.573938580966287, 1.054640095868647, 0.608288170918009;
        0.139722116114900, 0.564521020333593, 0.608288170918009, 1.710592047297405];

    const b_im: Matrix4<f64> = matrix![
        0.000000000000000,  0.084188116589642, 0.020945913983118, 0.014889726559839;
        -0.084188116589642, 0.000000000000000, 0.077767726663827, 0.186323360692751;
        -0.020945913983118,-0.077767726663827, 0.000000000000000, 0.391241589880123;
        -0.014889726559839,-0.186323360692751,-0.391241589880123, 0.000000000000000];

    const ab_re: Matrix4<f64> = matrix![
        1.519349110730174, 1.917906768872737, 1.884242091517871, 2.213745461494134;
        1.202116160610222, 2.011145474553397, 1.724795802329653, 2.278971295533136;
        0.959855217583175, 1.127721178454109, 0.990113697210399, 0.992891227415800;
        1.297802316052919, 1.861748214237581, 1.532221307540991, 2.167644075393497];

    const ab_im: Matrix4<f64> = matrix![
        -2.957939325646674e-02, -1.199536003759932e-01, -6.335090965608253e-02, 4.572905722670755e-01;
         2.959724080455181e-03, -1.227614136079116e-01, -2.292666130059569e-01, 3.426810100726049e-01;
        -2.931981596729040e-01, -6.832813683274620e-02, -1.261116768979296e-01, 1.847564343683648e-01;
        -6.961171587945102e-02, -8.411007881154724e-02, -2.290896302122995e-01, 2.784524837623079e-01];

    const as_b_re: Matrix4<f64> = matrix![
        -1.282293890603552e-01, -1.238171927555839e-01, -4.397696635999512e-01, 3.138538891880276e-01;
        -1.991274905384819e-01, -1.463907104775624e-01, -2.360313041924568e-01, 3.864818611305487e-01;
         1.917538457267177e-01,  3.208165428080038e-02,  2.643443186889143e-02, 2.621686756630002e-01;
        -1.023976710879659e-01, -1.331007909043816e-01, -2.729927469603199e-01, 2.481856676690262e-01];

    const as_b_im: Matrix4<f64> = matrix![
        -1.527754785802975, -1.883324040975753, -1.873907773832494, -2.362967730692886;
        -1.203548888789652, -2.038119878666596, -1.739327216570983, -2.262539734395240;
        -0.955953473551766, -1.091031670165521, -0.992562615801931, -0.995037484177228;
        -1.295190240405435, -1.861435880475748, -1.534556471657165, -2.181342867582754];

    const ab_e: Vector4<f64> = vector![
        -0.379284660719176,
        0.231924664088539,
        0.563564249660948,
        1.514431308866931
    ];
    const b_e: Vector4<f64> = vector![
        0.403671602248660,
        0.746473805199097,
        1.118109007864207,
        2.740521446909593
    ];

    #[test]
    fn zhemm_herm() {
        try_init().unwrap();
        let elpa = ElpaInstanceBuilder::try_new(4, 4).unwrap();
        let mut elpa = elpa.try_setup().unwrap();

        let a = Matrix4::from_fn(|i, j| a_re[(i, j)] + a_im[(i, j)] * I);
        let b = Matrix4::from_fn(|i, j| b_re[(i, j)] + b_im[(i, j)] * I);
        let ab = Matrix4::from_fn(|i, j| ab_re[(i, j)] + ab_im[(i, j)] * I);
        // eprintln!("A = {a:18.15}");
        // eprintln!("B = {b:18.15}");

        use crate::consts::ZgemmChar::AllMatrix;
        let check_zgemm = {
            let mut c_work = b.clone();
            elpa.zgemm_ad(
                4,
                a.as_slice(),
                AllMatrix,
                b.as_slice(),
                c_work.as_mut_slice(),
                AllMatrix,
            )
            .unwrap();

            // eprintln!("\nC = A^+ * B\n{c_work:18.15}");
            (c_work - ab).norm()
        };
        assert!(check_zgemm < 1e-13);
        try_uninit().unwrap();
    }

    #[test]
    fn zhemm_herm_skew() {
        try_init().unwrap();
        let elpa = ElpaInstanceBuilder::try_new(4, 4).unwrap();
        let mut elpa = elpa.try_setup().unwrap();

        let a_skew = Matrix4::from_fn(|i, j| a_im[(i, j)] + a_re[(i, j)] * I);
        let b = Matrix4::from_fn(|i, j| b_re[(i, j)] + b_im[(i, j)] * I);
        let as_b = Matrix4::from_fn(|i, j| as_b_re[(i, j)] + as_b_im[(i, j)] * I);
        // eprintln!("A_SKEW = {a_skew:18.15}");
        // eprintln!("B = {b:18.15}");

        use crate::consts::ZgemmChar::AllMatrix;
        let check_zgemm = {
            let mut c_work = b.clone();
            elpa.zgemm_ad(
                4,
                a_skew.as_slice(),
                AllMatrix,
                b.as_slice(),
                c_work.as_mut_slice(),
                AllMatrix,
            )
            .unwrap();

            // eprintln!("\nC = A_SKEW^+ * B\n{c_work:18.15}");
            (c_work - as_b).norm()
        };
        assert!(check_zgemm < 1e-13);
        try_uninit().unwrap();
    }

    #[test]
    fn inv_cholesky() {
        try_init().unwrap();
        let elpa = ElpaInstanceBuilder::try_new(4, 4).unwrap();
        let mut elpa = elpa.try_setup().unwrap();

        let a = Matrix4::from_fn(|i, j| a_re[(i, j)] + a_im[(i, j)] * I);
        let b = Matrix4::from_fn(|i, j| b_re[(i, j)] + b_im[(i, j)] * I);
        // eprintln!("B = {b:18.15}");

        let mut evec = Matrix4::zeros();
        let mut eval = Vector4::zeros();
        let check_chol = {
            let mut a_work = a.clone();
            let mut b_work = b.clone();
            elpa.eigvec_generalized(
                a_work.as_mut_slice(),
                b_work.as_mut_slice(),
                false,
                evec.as_mut_slice(),
                eval.as_mut_slice(),
            )
            .unwrap();

            // eprintln!("B = U^T U\ninv(U) = {b_work:18.15}");
            (b_work * b_work.adjoint() * b - Matrix4::identity()).norm()
        };
        assert!(check_chol < 1e-15);
        try_uninit().unwrap();
    }

    #[test]
    fn eigen() {
        try_init().unwrap();
        let elpa = ElpaInstanceBuilder::try_new(4, 4).unwrap();
        let mut elpa = elpa.try_setup().unwrap();

        let b = Matrix4::from_fn(|i, j| b_re[(i, j)] + b_im[(i, j)] * I);
        // eprintln!("B = {b:18.15}");

        let mut evec = Matrix4::zeros();
        let mut eval = Vector4::zeros();

        let mut b_work = b.clone();
        elpa.eigvec(
            b_work.as_mut_slice(),
            evec.as_mut_slice(),
            eval.as_mut_slice(),
        )
        .unwrap();

        // eprintln!("B C = C E\nE = {eval:18.15}\nC = {evec:18.15}");
        assert!((eval - b_e).norm() < 1e-14);
        assert!((b * evec - evec * Matrix4::from_diagonal(&eval.cast::<c64>())).norm() < 1e-15);
        try_uninit().unwrap();
    }

    #[test]
    fn eigen_transform() {
        try_init().unwrap();
        let elpa = ElpaInstanceBuilder::try_new(4, 4).unwrap();
        let mut elpa = elpa.try_setup().unwrap();

        let b = Matrix4::from_fn(|i, j| b_re[(i, j)] + b_im[(i, j)] * I);
        // eprintln!("B = {b:18.15}");

        let mut evec = Matrix4::zeros();
        let mut eval = Vector4::zeros();

        let mut b_work = b.clone();
        elpa.eigvec(
            b_work.as_mut_slice(),
            evec.as_mut_slice(),
            eval.as_mut_slice(),
        )
        .unwrap();

        // eprintln!("B C = C E\nE = {eval:18.15}\nC = {evec:18.15}");
        use crate::consts::ZgemmChar::AllMatrix;

        // since B^+ = B, one has C^+ B^+ C = C^+ (B^+ C)
        let mut bc = b.clone();
        let mut cbc = b.clone();
        elpa.zgemm_ad(
            4,
            b.as_slice(),
            AllMatrix,
            evec.as_slice(),
            bc.as_mut_slice(),
            AllMatrix,
        )
        .unwrap();
        elpa.zgemm_ad(
            4,
            evec.as_slice(),
            AllMatrix,
            bc.as_slice(),
            cbc.as_mut_slice(),
            AllMatrix,
        )
        .unwrap();
        // eprintln!("C^+ B C = {cbc:18.15}");
        assert!((cbc - Matrix4::from_diagonal(&eval.cast::<c64>())).norm() < 1e-14);
        try_uninit().unwrap();
    }

    #[test]
    fn eigen_generalized() {
        try_init().unwrap();
        let elpa = ElpaInstanceBuilder::try_new(4, 4).unwrap();
        let mut elpa = elpa.try_setup().unwrap();

        let a = Matrix4::from_fn(|i, j| a_re[(i, j)] + a_im[(i, j)] * I);
        let b = Matrix4::from_fn(|i, j| b_re[(i, j)] + b_im[(i, j)] * I);
        // eprintln!("A = {a:18.15}");
        // eprintln!("B = {b:18.15}");

        let mut evec = Matrix4::zeros();
        let mut eval = Vector4::zeros();

        let mut a_work = a.clone();
        let mut b_work = b.clone();
        elpa.eigvec_generalized(
            a_work.as_mut_slice(),
            b_work.as_mut_slice(),
            false,
            evec.as_mut_slice(),
            eval.as_mut_slice(),
        )
        .unwrap();

        // eprintln!("A C = B C E\nE = {eval:18.15}\nC = {evec:18.15}");
        assert!((eval - ab_e).norm() < 1e-14);
        assert!((a * evec - b * evec * Matrix4::from_diagonal(&eval.cast::<c64>())).norm() < 1e-15);
        try_uninit().unwrap();
    }

    #[test]
    fn eigen_generalized_transform() {
        try_init().unwrap();
        let elpa = ElpaInstanceBuilder::try_new(4, 4).unwrap();
        let mut elpa = elpa.try_setup().unwrap();

        let a = Matrix4::from_fn(|i, j| a_re[(i, j)] + a_im[(i, j)] * I);
        let b = Matrix4::from_fn(|i, j| b_re[(i, j)] + b_im[(i, j)] * I);
        // eprintln!("A = {a:18.15}");
        // eprintln!("B = {b:18.15}");

        let mut evec = Matrix4::zeros();
        let mut eval = Vector4::zeros();

        let mut a_work = a.clone();
        let mut b_work = b.clone();
        elpa.eigvec_generalized(
            a_work.as_mut_slice(),
            b_work.as_mut_slice(),
            false,
            evec.as_mut_slice(),
            eval.as_mut_slice(),
        )
        .unwrap();

        // eprintln!("A C = B C E\nE = {eval:18.15}\nC = {evec:18.15}");
        use crate::consts::ZgemmChar::AllMatrix;

        let mut ac = b.clone();
        let mut cac = b.clone();
        elpa.zgemm_ad(
            4,
            a.as_slice(),
            AllMatrix,
            evec.as_slice(),
            ac.as_mut_slice(),
            AllMatrix,
        )
        .unwrap();
        elpa.zgemm_ad(
            4,
            evec.as_slice(),
            AllMatrix,
            ac.as_slice(),
            cac.as_mut_slice(),
            AllMatrix,
        )
        .unwrap();

        let mut bc = b.clone();
        let mut cbc = b.clone();
        elpa.zgemm_ad(
            4,
            b.as_slice(),
            AllMatrix,
            evec.as_slice(),
            bc.as_mut_slice(),
            AllMatrix,
        )
        .unwrap();
        elpa.zgemm_ad(
            4,
            evec.as_slice(),
            AllMatrix,
            bc.as_slice(),
            cbc.as_mut_slice(),
            AllMatrix,
        )
        .unwrap();

        // eprintln!("C^+ A C = {cac:18.15}");
        // eprintln!("C^+ B C = {cbc:18.15}");
        assert!((cac - Matrix4::from_diagonal(&eval.cast::<c64>())).norm() < 1e-14);
        assert!((cbc - Matrix4::identity()).norm() < 1e-14);
        try_uninit().unwrap();
    }
}

mod autotune {
    #![allow(non_upper_case_globals)]
    use crate::{c64, try_init, try_uninit, ElpaInstanceBuilder};
    use nalgebra::{DMatrix, DVector};

    fn set_random_hermitian_matrix(size: usize) -> DMatrix<c64> {
        const I: c64 = c64::new(0., 1.);

        let re = DMatrix::<f64>::new_random(size, size).symmetric_part();
        let mut im = DMatrix::<f64>::new_random(size, size).symmetric_part();
        im.fill_lower_triangle(0., 0);
        let im = im.clone() - im.adjoint();
        re.cast::<c64>() + im.cast::<c64>() * I
    }

    generate_test!(mat50_iter25, 50, 25);
    generate_test!(mat100_iter25, 100, 25);
    generate_test!(mat200_iter25, 200, 25);
    generate_test!(mat500_iter25, 500, 25);
    generate_test!(mat1000_iter25, 1000, 25);
    //generate_test!(mat2000_iter15, 2000, 15);
    //generate_test!(mat5000_iter10, 5000, 10);
    //generate_test!(mat10000_iter5, 10000, 5);

    macro_rules! generate_test {
        ($name:ident, $matrix_size:expr, $iter_num:expr) => {
            #[test]
            fn $name() {
                const MATRIX_SIZE: usize = $matrix_size;
                const ITER_NUM: usize = $iter_num;

                let mut elpa_results = DVector::zeros(ITER_NUM);

                try_init().unwrap();
                let elpa = ElpaInstanceBuilder::try_new(MATRIX_SIZE, MATRIX_SIZE).unwrap();
                let mut elpa = elpa.try_setup().unwrap();
                let mut tuner = elpa
                    .try_autotune_setup(
                        crate::AutotuneLevel::Extensive,
                        crate::AutotuneDomain::Complex,
                    )
                    .unwrap();
                elpa.try_autotune_print_state(&tuner).unwrap();

                let mut evec = DMatrix::zeros(MATRIX_SIZE, MATRIX_SIZE);
                let mut eval = DVector::zeros(MATRIX_SIZE);

                eprintln!("{:^8} {:^12}", "ITER", "ELPA TIME");

                let mut break_flag = false;
                loop {
                    for i in 0..ITER_NUM {
                        let m = set_random_hermitian_matrix(MATRIX_SIZE);

                        let mut work = m.clone();

                        let etime = std::time::Instant::now();
                        elpa.eigvec(
                            work.as_mut_slice(),
                            evec.as_mut_slice(),
                            eval.as_mut_slice(),
                        )
                        .unwrap();
                        let etime = etime.elapsed().as_secs_f64();

                        if !break_flag {
                            elpa.try_autotune_step(&mut tuner).unwrap();
                        }

                        eprintln!("{i:^8} {etime:^12.3e}");
                        elpa_results[i] = etime;
                    }

                    eprintln!(
                        "{:^12} {:^12} {:^12} {:^12} {:^12}",
                        "Time, secs", "MEAN", "VARIANCE", "WORST", "BEST"
                    );
                    eprintln!(
                        "{:>12} {:^12.2e} {:^12.2e} {:^12.2e} {:^12.2e}",
                        "ELPA",
                        elpa_results.mean(),
                        elpa_results.variance(),
                        elpa_results.max(),
                        elpa_results.min()
                    );

                    if !break_flag {
                        elpa.try_autotune_print_state(&tuner).unwrap();
                        elpa.try_autotune_set_best(&mut tuner).unwrap();
                        break_flag = true;
                        continue;
                    }

                    break;
                }

                try_uninit().unwrap();
            }
        };
    }

    pub(self) use generate_test;
}
